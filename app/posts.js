const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const createRouter = (db) => {

  router.get('/', (req, res) => {
    res.send(db.getPosts());
  });

  router.post('/', upload.single('image'), (req, res) => {
    const post = req.body;

    if (req.file) {
      post.image = req.file.filename;
    }

    if (post.author === '') {
      post.author = 'Anonymous';
    }

    if (post.message === '') {
      res.status(400).send({ error: "Field Message can't be blank" });
    } else {
      db.addItem(post).then(result => {
        res.send(result);
      })
    }

  });

  return router;
};

module.exports = createRouter;